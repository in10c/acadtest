<!DOCTYPE html>
	<html lang='es'>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Sistema de aplicación de examenes">
		<meta name="author" content="Cristian Orihuela Torres">
		<title>AcadTest - Error en los datos de acceso</title>
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/estilo.css" rel="stylesheet">
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<section class="row">
			<article class="medRight col-md-5 text-center">
				<div class="alert alert-danger">
				<h2>Servicio denegado</h2>
				No tienes permiso para acceder al contenido de esta página.
				<br><br>
				<img src="../imagenes/locked.png" />
				<br><br>
				<a href="index.php" class="btn btn-danger">Acceso a docentes</a>
				</div>
			</article>
		</section>
	</body>
	</html>
