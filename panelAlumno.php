<?php
	include "motor/controlador.php";
	$metodosControlador = new Controlador;
	session_start("aluAcadTest");
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de aplicación de examenes">
    <meta name="author" content="Cristian Orihuela Torres">
    <title>AcadTest - Sistema de aplicación de exámenes en línea</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<section class="row">
	<article class="medRight col-md-5">
		<div class="container">
			<h2 class="text-center"><span class="text-info"> ~( acad</span>Test )~ </h2>
			<br>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Selección de prueba</h3>
			  </div>
			  <div class="panel-body">
			    <form id="escogeExamen" action="aplicaPrueba.php" role="form"  method="POST">
			    	Bienvenido(a) <span class="text-info">
			    	<?php
						$metodosControlador->verificarNombre();
					?></span>, selecciona una prueba para responder. <br><br>
				  <div class="form-group">
				    <label>Materia</label>
				    <?php
						$metodosControlador->selecMatSimple();
					?>
				  </div>
				  <div id="selectExamen" class="form-group">
				    <label>Examen</label>
				    <select required class="form-control">
				    	<option></option>
				    </select>
				  </div>
				  <div class="form-group">
				    <label>Contraseña de acceso</label>
				    <input type="password" required class="form-control" name="contrasenaExamen" placeholder="Contraseña">
				  </div>
				  <div class="btn-group pull-right">
					  <a id="cerrarSesion" href="#" class="btn btn-default"> Cerrar Sesión</a>
					  <button type="submit" class="btn btn-primary">Acceder</button>
				  </div>
				  
				</form>
				  <!-- Modal -->
				  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				      <div class="modal-content">
				        <div class="modal-header">
				          
				          <h4 class="modal-title">Nombre de usuario</h4>
				        </div>
				        <div class="modal-body">
				        	
					          Parece que te acabas de registrar, porfavor proporciónanos tu nombre. <br><br>
					          <input type="text" id="nombreUser" required class="form-control">
				        	
				        </div>
				        <div class="modal-footer">
				          <button type="button" id="guardaNom" class="btn btn-primary">Guardar</button>
				        </div>
				      </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				  </div><!-- /.modal -->
				
			  </div>
			</div>
		</div>		
	</article>
</section>
</body>
</html>

<script type="text/javascript">
$(document).on("ready", iniciar);

function iniciar() {
	$("#selecMatSimple").change(cambioMat);
	$.get("motor/ajax.php?seccion=verificarNombre", pedirNombre);
	$("#cerrarSesion").on("click", cerrarSesion);
}

function pedirNombre (retorno) {
	if (retorno=="") {
		$('#myModal').modal("show");
		$("#guardaNom").on("click", guardarNombre);
	};
}

function cerrarSesion () {
	$.get("motor/ajax.php?seccion=cerrarSesion", function(){
    		window.location = "index.php";
    	});
}

function cambioMat () {
	datos = "id="+$(this).val();
	contenedorExam = $("#selectExamen");
	$.get("motor/ajax.php?seccion=mostrarSelectorExa", datos, actualizaExam);
}

function guardarNombre () {
 datos= $("#nombreUser").val();
 if (datos=="") {
 	alert("Porfavor llena el campo con tu nombre completo.");
 } else {
 	$.get("motor/ajax.php?seccion=guardarNombre", "nombre="+datos, recargar);	
 }
}

function recargar (){
	location.reload();
}

function actualizaExam(retorno) {
	contenedorExam.html(retorno);
}
</script>