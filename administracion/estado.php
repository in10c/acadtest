<?php
	include "cabeza.php";
	include "../motor/controlador.php";
	$metodosControlador = new Controlador;
	$metodosControlador->comprobarLogin();
	$idExamen= $_GET["id"];
  	$nomEx = $metodosControlador->obtenerNombreExamen($idExamen);
?>

<h2> <?php echo $nomEx; ?> |<span class="subtitulo"> Permisos de acceso </span></h2>
<p class="masSeparacionY">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<div class="row masSeparacionY">
	<div class="col-md-5">
		<legend>Estado del examen</legend>
		<p id="controlActivaciones">
			<?php
			$metodosControlador->mostrarControlActivacionExamenes($idExamen);
			?>
		</p>
		<br>
			<div id="labelContra">
				<?php
					$salida =$metodosControlador->contrasenaExamen($idExamen);
					if ($salida!="NOTSET") {
						?>
						<p>
							<span class="subtitulo">Contraseña
								<span class="text-info pull-right">
									<?php
										echo $salida;
									?>
								</span>
							</span>
						</p>	
						<?php
					}
				?>
		</div>		
	</div>
	<div class="col-md-7 ">
		<legend><span style="font-size:15px;" class="glyphicon glyphicon-cog"></span> Opciones</legend>
		<div class="row">
			<div class="col-md-8">
				<span class="subtitulo">Preguntas a mostrar</span>
			</div>
			<div id="controlNumero" class="col-md-4">
				<?php
				$metodosControlador->selectorNumeroPreguntasMostrar($idExamen);
				?>
			</div>
		</div>
	</div>
</div>

<?php 
	include("pie.php");
?>

<script type="text/javascript">
$(document).on("ready", iniciar);

function iniciar () {
	$(".cambiarEstado").on("click", cambiarEstado);
	$("#selectorNumeroPreg").change(guardarLimite);
}
function cambiarEstado (){
	id = $(this).data("id");
	datos = "id="+id+"&ahora="+$(this).data("ahora");
	$.get("../motor/ajax.php?seccion=cambiarEstado", datos, actualizarControl);
}
function actualizarControl(retorno) {	
	if (retorno=="OK") {
		$("#controlActivaciones").load("../motor/ajax.php?seccion=mostrarControlActivacionExamenes", "id="+id, function(){
			$(".cambiarEstado").on("click", cambiarEstado);
			$.get("../motor/ajax.php?seccion=actualContra", "id="+id, function(retorno){
				$("#labelContra").html(retorno);
			});
		});
	} else {
	    alert("Ha ocurrido un error, favor de avisar al administrador.");
  	}
}
function guardarLimite() {
	datos = "id="+$(this).data("id")+"&numero="+$(this).val();
	//alert(datos);
	$.get("../motor/ajax.php?seccion=cambiarLimite", datos, actualizarCtlNumeros);
}

function actualizarCtlNumeros(retorno){
	if (retorno!="OK") {
		alert("Algo ha salido mal, favor de avisar al administrador.");
	};
}
</script>