<?php
	include "cabeza.php";
  include "../motor/controlador.php";
  $metodosControlador = new Controlador;
  $metodosControlador->comprobarLogin();
?>

<h2>Registrar nuevo examen</h2>
<form role="form" id="nuevoExamen" action="../motor/ajax.php?seccion=registrarExamen" method="post">
  <div class="form-group">
    <label>Nombre del examen</label>
    <input type="text" class="form-control" name="nombre" placeholder="Ejemplo: examen segunda unidad" required>
  </div>
  <div class="form-group">
    <label>Introducción</label>
    <textarea class="form-control" name="intro" id="cuadroIntroRegistrar" rows="5"></textarea>
  </div>
  <div class="form-group">
    <label>Agregar a materias</label>
    <div id="selectorMaterias">
      <?php
        $metodosControlador->seleccionarMaterias();
      ?>
    </div>
  </div>
  <p id="nuevasMaterias">
    <a class="btn btn-default" id="nuevaMateria" href="#">Nueva materia</a>
  </p>
  <div id="avisos"></div>
  <button type="submit" class="btn btn-success pull-right">Submit</button>
</form>

<?php
	include("pie.php");
?>

<script type="text/javascript">
  $(document).on("ready", iniciar);

  function iniciar() {
    contenedor = $("#nuevasMaterias");
    avisos = $("#avisos");
    $("#nuevaMateria").on("click", nuevaMateria);
  }

  //-----------------------------------------------------
  // Funciones Materias 
  //-----------------------------------------------------

  function nuevaMateria(){
    
    contenedor.slideUp("fast", function(){
      contenedor.html("<div class='form-group'>"
                      +"<input type='text' id='guardarMateria' class='form-control' placeholder='Coloca el nombre de la materia y presiona la tecla ENTER'> "
                      +"</div>");
      $("#guardarMateria").keypress(guardarMateria);
      contenedor.slideDown("fast");
    });
  }
  function guardarMateria (e){
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code==13){
        var datos = "nombre="+$(this).val(); 
        $.get("../motor/ajax.php?seccion=guardarMateria", datos, mostrarMaterias);
        return false;
     } 
  }
  function mostrarMaterias(ab){
    contenedor.html('<a class="btn btn-default" id="nuevaMateria" href="#">Nueva materia</a>');
    $("#selectorMaterias").load("../motor/ajax.php?seccion=seleccionarMaterias");
    $("#nuevaMateria").on("click", nuevaMateria);
    if (ab=="OK"){
      avisos.append("<div class='alert alert-success alert-dismissable'>" 
        +"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> "
        +"Materia guardada correctamente.</div>");
    } else {
      avisos.append("<div class='alert alert-danger alert-dismissable'>" 
        +"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> "
        +"Error al guardar la materia, favor de avisar al administrador. </div>");
      console.log(ab);
    }
  }
</script>