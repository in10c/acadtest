<?php
	include "cabeza.php";
  include "../motor/controlador.php";
  $metodosControlador = new Controlador;
  $metodosControlador->comprobarLogin();
  $idExamen= $_GET["id"];
  $nomEx = $metodosControlador->obtenerNombreExamen($idExamen);

  if (isset($idExamen)){
?>

<h2> <?php echo $nomEx; ?> | <span class="subtitulo">Editar preguntas y respuestas</span> </h2>
<div class="pull-right">
   <a href="adminExamen.php" class="btn btn-default btn-lg">Ir al administrador</a>
   <a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Nueva pregunta</a>
</div>

<div id="tabla">
  <?php $metodosControlador->tablaDePreguntas($idExamen); ?>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Nueva pregunta</h4>
      </div>
      <div class="modal-body">
        <?php
          echo '<form role="form" id="registrarPregunta" data-idExamen="'.$idExamen.'">';
        ?>
          <div class="input-group">
            <span class="input-group-addon">¿</span>
            <input type="text" class="form-control" name="pregunta" required>
            <span class="input-group-addon">?</span>
          </div>
          <br><h4>Respuestas:</h4>
          <div class="row">
            <div class="col-md-2">
              <div id="columnaContadores" class="pull-right">
                <p>
                  <span class="badge">1</span></a>
                </p>
                <p>
                  <span class="badge">2</span></a>
                </p>
              </div>
            </div>
            <div class="col-md-9">
              <div id="columnaRespuestas">
                <p>
                  <input data-numero="1" name="respuesta[]" data-condicion="incorrecta" class="respuesta form-control" required>
                </p>
                <p>
                  <input data-numero="2" name="respuesta[]" data-condicion="incorrecta" class="respuesta form-control" required>
                </p>
              </div>
            </div>
            <div class="col-md-1">
              <div id="columnaCheck">
                <p>
                  <a data-numero="1" data-toggle="tooltip" class="conInfo" title="Marcar como respuesta correcta" href="#"><span class="glyphicon glyphicon-ok-sign"></span></a>
                </p>
                <p>
                  <a data-numero="2" data-toggle="tooltip" class="conInfo" title="Marcar como respuesta correcta" href="#"><span class="glyphicon glyphicon-ok-sign"></span></a>
                </p>
              </div>
            </div>
          </div>
        
        <a href="#" id="nuevaRespuesta" class="pull-right"><span class="glyphicon glyphicon-plus-sign"></span>    Añadir respuesta</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Guardar pregunta y respuestas</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php
  }
  else {
    echo "<h2>Error! No elegiste examen a editar</h2>";
  }

	include("pie.php");
?>

<script type="text/javascript">
$(document).on("ready", iniciar);

function iniciar () {
  contador = 2;
  idExamen = $("#registrarPregunta").data("idexamen");
  columnaContadores = $("#columnaContadores");
  columnaRespuestas = $("#columnaRespuestas");
  columnaCheck = $("#columnaCheck");
  $("#nuevaRespuesta").on("click", agregarRespuesta);
  $(".conInfo").on("click", marcarCorrecta);
  $("#registrarPregunta").on("submit", registrarPregunta);
  $(".eliminarPregunta").on("click", eliminarPregunta);
}

function agregarRespuesta(){
  contador+=1;
  columnaContadores.append('<p><span data-numero="'+contador+'" class="badge">'
    +contador+'</span></a></p>');
  columnaRespuestas.append('<p><input data-condicion="incorrecta" name="respuesta[]" required data-numero="'+contador+'" class="respuesta form-control"></p>');
  columnaCheck.append('<p><a data-numero="'+contador+'" data-prueba="65" class="conInfo" data-toggle="tooltip" title="Marcar como respuesta correcta" '
    +'href="#"><span class="glyphicon glyphicon-ok-sign"></span></a></p>');
  $(".conInfo").on("click", marcarCorrecta);
}

function marcarCorrecta(){
  numero = $(this).data("numero");
  $(".conInfo").each(function(){
    $(this).removeClass("activo");
  })
  $(this).addClass("activo");
  $(".respuesta").each(function(){
    if ($(this).data("numero") == numero) {
      $(this).attr('data-condicion', 'correcta');
    } else {
      $(this).attr('data-condicion', 'incorrecta');
    }
  });
}

function registrarPregunta(){
  
  respuestaCorrecta=0;
  correcto = 0;
  $(".respuesta").each(function(){
    if ($(this).attr("data-condicion") == "correcta") {
      respuestaCorrecta+=1;
      correcto = $(this).attr("data-numero");
    } 
  });
  if(respuestaCorrecta!=0){
    datos = $(this).serialize()+"&correcto="+correcto+"&idExamen="+idExamen; 
    $.get("../motor/ajax.php?seccion=registrarPregunta", datos, actualizarTabla); 
    return false;
  } else {
    alert("No elegiste una respuesta correcta, tienes que hacerlo.");
    return false;
  }
}

function actualizarTabla(retorno){
  //alert(retorno);
  if(retorno =="OK"){
    $("#myModal").modal("hide");
    recargaTabla();
  } else {
    alert("Ocurrió un error, favor de avisar al administrador.");
  }
}

function recargaTabla(){
  $("#tabla").slideUp("fast", function(){
      datos = "idExamen="+idExamen;
      //alert(datos);
      $(this).load("../motor/ajax.php?seccion=tablaDePreguntas", datos, function(){
        $(this).slideDown("fast");
        $(".eliminarPregunta").on("click", eliminarPregunta);
      });
    });

}

function eliminarPregunta(){
  datos = "id="+$(this).data("id");
  if (confirm("¿Estás seguro que deseas borrar esta pregunta?")){
    $.get("../motor/ajax.php?seccion=eliminarPregunta", datos, function(ab){
      if (ab=="OK"){
        recargaTabla();
      } else {
        alert("Ocurrió un error, favor de avisar al administrador.");
      }
    });
  }
}
</script>