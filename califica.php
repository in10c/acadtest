<?php
	include "motor/controlador.php";
	$metodosControlador = new Controlador;
	$idExamen = $_GET["idExamen"];
	$idAlumno = $_GET["idAlumno"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de aplicación de examenes">
    <meta name="author" content="Cristian Orihuela Torres">
    <title>AcadTest - Sistema de aplicación de exámenes en línea</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<section class="row">
	<article class="col-md-8 col-md-offset-2">
		<div class="container">
			<h2 class="text-center"><span class="text-info"> ~( acad</span>Test )~ </h2>
			<br>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Calificación del examen / <span class="text-muted"><?php $metodosControlador->nombreExamen($idExamen); ?></span></h3>
			  </div>
			  <div class="panel-body">
			  	<div class="alert alert-info">
				  	Alumno : <b><?php $metodosControlador->obtenNombreAlu($idAlumno); ?></b> <br>
				  	Total de aciertos : <b> <?php $metodosControlador->aciertos($idExamen, $idAlumno); ?></b> <br>
				  	Total de preguntas : <b> <?php $metodosControlador->total($idExamen, $idAlumno); ?></b> <br>
				  	Calificación : <b> <?php 

				  	$numero = number_format($metodosControlador->calificaRet($idExamen, $idAlumno)); 
				  	echo $numero;

				  	?></b> 
			  	</div>
			  	<br>
			  	<?php $metodosControlador->tablaCalifica($idExamen, $idAlumno); ?>
			  </div>
			</div>
		</div>		
	</article>
</section>
</body>
</html>