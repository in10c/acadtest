<?php
	include "cabeza.php";
	include "../motor/controlador.php";
	$metodosControlador = new Controlador;
	$metodosControlador->comprobarLogin();
?>

<h2>Administrador de exámenes</h2>
Ver examenes de las siguientes materias:
<br><br>
<?php 
	$metodosControlador->seleccionarMaterias(); 
	echo "<br><div id='contenedorTabla'></div>";
	include("pie.php");
?>

<script type="text/javascript">
$(document).on("click", iniciar);

function iniciar(){
	$("#selectorMaterias").change(mostrarExamenes);
}

function mostrarExamenes() {
	datos = "ids="+$(this).val();
	$.get("../motor/ajax.php?seccion=mostrarExamenes", datos, actualizarTablaExamenes);
}
function actualizarTablaExamenes(retorno){
	$("#contenedorTabla").html(retorno);
	$(".borrarExamen").on("click", eliminarExamen);
}

function eliminarExamen() {
	datos = "id="+$(this).data("id")+"&matid="+$(this).data("matid");
	if (confirm("¿Estás seguro que deseas borrar este examen?")){
	$.get("../motor/ajax.php?seccion=eliminarExamen", datos, function(ab){
	  if (ab=="OK"){
	  	datos = "ids="+$("#selectorMaterias").val();
	    $.get("../motor/ajax.php?seccion=mostrarExamenes", datos, actualizarTablaExamenes);
	  } else {
	    alert("Ocurrió un error, favor de avisar al administrador.");
	  }
	});
	}
}
</script>