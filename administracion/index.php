<?php
	include "../motor/controlador.php";
	$metodosControlador = new Controlador;
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de aplicación de examenes">
    <meta name="author" content="Cristian Orihuela Torres">
    <title>AcadTest - Sistema de aplicación de exámenes en línea</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>
<section class="row">
	<article class="medRight col-md-5">
		<div class="container">
			<h2 class="text-center"><span class="text-info"> ~( acad</span>Test )~ </h2>
			<br>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Acceso para docentes</h3>
			  </div>
			  <div class="panel-body">
			    <form role="form" action="validar.php" method="POST">
				  <div class="form-group">
				    <label>Usuario</label>
				    <input type="text" required name="usuario" class="form-control" >
				  </div>
				  
				  <div class="form-group">
				    <label>Contraseña</label>
				    <input type="password" required name="contra" class="form-control" >
				  </div>
				  
				  <button type="submit" class="btn btn-primary pull-right">Acceder</button>
				</form>
			  </div>
			</div>
		</div>		
	</article>
</section>
</body>
</html>