<?php
	include "motor/controlador.php";
	$metodosControlador = new Controlador;
	session_start("aluAcadTest");
	$idExamen = $_POST["idExamen"];
	$contra = $_POST["contrasenaExamen"];
	if ($metodosControlador->validaDatosExamen($idExamen, $contra)) {
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de aplicación de examenes">
    <meta name="author" content="Cristian Orihuela Torres">
    <title>AcadTest - Sistema de aplicación de exámenes en línea</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<section class="row">
	<article class="col-md-8 col-md-offset-2">
		<div class="container">
			<h2 class="text-center"><span class="text-info"> ~( acad</span>Test )~ </h2>
			<br>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title"><?php $metodosControlador->nombreExamen($idExamen); ?></h3>
			  </div>
			  <div class="panel-body">
			  	<div class="alert alert-dismissable alert-info">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <b>¡Información!. </b><?php $metodosControlador->introExamen($idExamen); ?>
				</div>
					<?php 
					$metodosControlador->generaPreguntas($idExamen);
					?>
					<hr>
					<div class="row">
						<div class="col-md-10">
							<div class="progress progress-striped active">
							  <div id="barraProgreso" class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
							    <span class="sr-only">45% Complete</span>
							  </div>
							</div>
							<span id="textoPorcentaje" class="text-muted">0% completado</span>
						</div>
						<div class="col-md-2">
							<?php

								echo '<a id="calificar" data-idExamen="'.$idExamen.'" class="btn btn-primary btn-lg pull-right" href="#">Calificar</a>';
							?>
						</div>
					</div>
			  </div>
			</div>
		</div>		
	</article>
</section>
</body>
</html>

<?php

} else {
	echo "La clave proporcionada para el examen es incorrecta.";
}


?>

<script type="text/javascript">
$(document).on("ready", iniciar);

function iniciar() {
	$(".respuestas").on("click", responder);
	$("#calificar").on("click", calificar);
}

function responder (){
	datos = "idExamen="+$(this).attr("data-idExamen")+"&idPregunta="+$(this).attr("data-idPregunta")+"&idRespuesta="+$(this).val();
	$(this).closest(".cajaPregunta").attr("data-estado", "contestado");
	$.get("motor/ajax.php?seccion=guardarRespuesta", datos, guardarRespuesta);
	
}

function calificar () {
	idExamen = $(this).attr("data-idExamen");
	noc = 0;
	$(".cajaPregunta").each(function(){
		if ($(this).attr("data-estado")=="noContestado") {
			noc+=1;
		};
	});

	if (noc==0) {
		$.get("motor/ajax.php?seccion=idAlumno", direcciona);
		
	} else {
		alert("Aún no puedes ser calificado, faltan preguntas por contestar.");
		return false;
	}
}
function guardarRespuesta (retorno){
	actualizaProgreso();
}

function direcciona(retorno){
	location.href="califica.php?idExamen="+idExamen+"&idAlumno="+retorno;
}

function actualizaProgreso() {
	contador = 0;
	contestadas = 0;
	$(".cajaPregunta").each(function(){
		if ($(this).attr("data-estado")=="contestado") {
			contestadas+=1;
		};
		contador+=1;
	});
	porcentaje = (contestadas / contador)*100;
	//alert(porcentaje);
	$("#barraProgreso").css("width", porcentaje+"%");
	$("#textoPorcentaje").text(porcentaje+"% completado");
}
</script>