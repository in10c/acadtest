<?php
$seccion = $_GET["seccion"];
include "controlador.php";
$metodosControlador = new Controlador;

switch ($seccion) {
	case 'guardarMateria':
		$metodosControlador->guardarMateria($_GET['nombre']);
		break;
	case 'seleccionarMaterias':
		$metodosControlador->seleccionarMaterias();
		break;
	case 'registrarExamen':
		$metodosControlador->registrarExamen($_POST["nombre"],$_POST["intro"], $_POST["materias"]);
		break;
	case 'registrarPregunta':
		$metodosControlador->registrarPregunta($_GET["pregunta"], $_GET["respuesta"],$_GET["idExamen"], $_GET["correcto"]);
		break;
	case 'tablaDePreguntas':
		$metodosControlador->tablaDePreguntas($_GET["idExamen"]);
		break;
	case 'eliminarPregunta':
		$metodosControlador->eliminarPregunta($_GET["id"]);
		break;
	case 'mostrarExamenes':
		$metodosControlador->mostrarExamenes($_GET["ids"]);
		break;
	case 'eliminarExamen':
		$metodosControlador->eliminarExamen($_GET["id"],$_GET["matid"]);
		break;
	case 'mostrarControlActivacionExamenes':
		$metodosControlador->mostrarControlActivacionExamenes($_GET["id"]);
		break;
	case 'cambiarEstado':
		$metodosControlador->cambiarEstado($_GET["id"], $_GET["ahora"]);
		break;
	case 'cambiarLimite':
		$metodosControlador->cambiarLimite($_GET["id"], $_GET["numero"]);
		break;
	case 'cerrarSesion':
		$metodosControlador->eliminarSesion();
		break;
	case 'mostrarSelectorExa':
		$metodosControlador->mostrarSelectorExa($_GET["id"]);
		break;
	case 'actualContra':
		$salida =$metodosControlador->contrasenaExamen($_GET["id"]);
		if ($salida!="NOTSET") {
			?>
				<span class="subtitulo">Contraseña
					<span class="text-info pull-right">
						<?php
							echo $salida;
						?>
					</span>
				</span>
			<?php
		}
		break;
	case 'guardarNombre':
		$metodosControlador->guardarNombre($_GET["nombre"]);
		break;
	case 'verificarNombre':
		$metodosControlador->verificarNombre();
		break;
	case 'guardarRespuesta':
		$metodosControlador->guardarRespuesta($_GET["idExamen"], $_GET["idPregunta"], $_GET["idRespuesta"]);
		break;
	case 'idAlumno':
		$metodosControlador->obtenIdAlumno();
		break;
	default:
		# code...
		break;
}

?>