<?php
	include("cabeza.php");
	include "../motor/controlador.php";
	$metodosControlador = new Controlador;
	$metodosControlador->comprobarLogin();
?>
<h2>Exámenes activos</h2>
<p>
	Desde este módulo puedes ver todos los examenes activos en este momento.
</p><br>
	<?php
		$metodosControlador->infoExamenes();
	?>
</div>
<?php
	include("pie.php");
?>
<!-- <script type="text/javascript" src="js/cThread.js"></script> -->
<script type="text/javascript">
	$(document).on("ready", iniciar);
	function iniciar() {
		setInterval(reloj,1000);
	}
	function reloj(){
		$(".contadores").each(function(){
			horas = parseInt($(this).find("span.horas").text());
			minutos = parseInt($(this).find("span.minutos").text());
			segundos = parseInt($(this).find("span.segundos").text());
			siguienteSegundos = segundos+1;
			if (siguienteSegundos==60){

				siguienteSegundos=0;
				if ((minutos+1)==60) {
					siguienteMinutos = 0;
					siguienteHoras = horas +1;
				} else {
					siguienteMinutos = minutos+1;
					siguienteHoras = horas
				}
			
			} else {
				siguienteMinutos = minutos;
				siguienteHoras = horas;
			}

			$(this).find("span.horas").text(siguienteHoras);
			$(this).find("span.minutos").text(siguienteMinutos);
			$(this).find("span.segundos").text(siguienteSegundos);
		}) 
	}
</script>