<?php

class Controlador
{
	
	function __construct()
	{
		require ('bdClase.php');
		$bd_obj = new metodosbd;
		$bd_obj->conectar_bd();
	}
	function seleccionarMaterias() {
		echo '<select required multiple id="selectorMaterias" name="materias[]" class="form-control">';
		$respuesta = mysql_query("SELECT * FROM materias WHERE estado=1") or die(mysql_error());
		while ($materias=mysql_fetch_array($respuesta)){
			echo "<option value='".$materias["id"]."'>".$materias["nombre"]."</option>";
		}
		echo '</select>';
	}

	function generaPreguntas($idExamen) {
		$limite = $this->limiteExamen($idExamen); 
		$sql = 'SELECT * FROM preguntas WHERE idExamen='.$idExamen.' ORDER BY rand() LIMIT '.$limite;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$i=1;
		while ($pregunta=mysql_fetch_array($respuesta)){
			echo '
			<div class="cajaPregunta" data-estado="noContestado">
						<legend class="text-right">¿'.$pregunta["pregunta"].'?</legend>
						<div>';

			$dos = 'SELECT id, respuesta FROM respuestas WHERE idPregunta='.$pregunta["id"].' ORDER BY rand()';
			$retor = mysql_query($dos) or die(mysql_error());
			
			while ($celda=mysql_fetch_array($retor)){
				echo '<div class="radio">
							  <label>
							    <input type="radio" class="respuestas" name="responde'.$i.'" data-idExamen="'.$idExamen.'" data-idPregunta="'.$pregunta["id"].'" value="'.$celda["id"].'" >
							    '.$celda["respuesta"].'
							  </label>
							</div>';
							
			}
							
			echo '			</div> 
					</div>
			'; $i+=1;
		}
	}

	function guardarRespuesta ($idExamen, $idPregunta, $idRespuesta){
		session_start();
		$sql = 'INSERT INTO respuestasAlumnos(idExamen, idPregunta, idRespuesta, idAlumno) VALUES ('.$idExamen.','.$idPregunta.','.$idRespuesta.','.$_SESSION["idAlumno"].')';
		$respuesta = mysql_query($sql) or die(mysql_error());
		echo "OK";
	}

	function mostrarSelectorExa ($id) {
	  	$retorno = mysql_query("SELECT * FROM detalleMateria WHERE idMateria=".$id) or die(mysql_error());
		echo '<label>Examen</label> <select required name="idExamen" class="form-control">';
		while ($celda=mysql_fetch_array($retorno)){
			$nomEx = $this->obtenerNombreExamen($celda["idExamen"]);
			if ($this->estaActivo($celda["idExamen"])) {
				echo "<option value='".$celda["idExamen"]."'>".$nomEx."</option>";
			}
		} echo '</select>';
	}

	function validaDatosExamen($idExamen, $contra) {
		$respuesta= mysql_query('SELECT id FROM examenes WHERE id='.$idExamen.' AND clave="'.$contra.'"');
		if (mysql_num_rows($respuesta)>0){
			return true;
		} else {
			return false;
		}
	}

	function nombreExamen($idExamen) {
		$sql = "SELECT nombre FROM examenes WHERE id=".$idExamen;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		echo $celda["nombre"];
	}

	function introExamen($idExamen) {
		$sql = "SELECT intro FROM examenes WHERE id=".$idExamen;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		echo $celda["intro"];
	}
	function limiteExamen($idExamen) {
		$sql = "SELECT limite FROM examenes WHERE id=".$idExamen;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["limite"];
	}

	function login($usuario, $contrasena) {
		$respuesta= mysql_query('SELECT id, usuario FROM profesores WHERE usuario="'.$usuario.'" AND clave="'.$contrasena.'"');
		if (mysql_num_rows($respuesta)>0){
			return true;
		}
		$this->eliminarSesion();
		return false;
	}

	function eliminarSesion() {

		ini_set("session.use_cookies", 1);
		ini_set("session.use_only_cookies", 1);
		session_start();

		$_SESSION = array();

		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}
		session_destroy();
	}

	function guardarNombre ($nombre) {
		session_start();
		$sql = 'UPDATE alumnos SET nombre="'.$nombre.'" WHERE id='.$_SESSION["idAlumno"];
		$respuesta = mysql_query($sql) or die(mysql_error());
		echo $sql;
	}

	function verificarNombre (){
		session_start();
		$sql = "SELECT nombre FROM alumnos WHERE id=".$_SESSION["idAlumno"];
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		echo $celda["nombre"];
	}
	function obtenNombreAlu ($id){
		session_start();
		$sql = "SELECT nombre FROM alumnos WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		echo $celda["nombre"];
	}
	function obtenNombreAluRet ($id){
		session_start();
		$sql = "SELECT nombre FROM alumnos WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["nombre"];
	}

	function comprobarLogin() {
		session_start();
		if (!isset($_SESSION['logued'])){
			session_destroy(); 
			header('Location:sinPermiso.php');
			break;
		}
	}

	function obtenIdAlumno() {
		session_start();
		echo $_SESSION["idAlumno"];
	}

	function selecMatSimple() {
		echo '<select required id="selecMatSimple" name="materias" class="form-control">';
			$respuesta = mysql_query("SELECT * FROM materias WHERE estado=1") or die(mysql_error());
			while ($materias=mysql_fetch_array($respuesta)){
				echo "<option value='".$materias["id"]."'>".$materias["nombre"]."</option>";
			}
		echo '</select>';
	}
	function guardarMateria($nombre){
		$respuesta = mysql_query("INSERT INTO materias (nombre) VALUES ('$nombre')") or die(mysql_error());
		echo "OK";
	}
	function registrarExamen($nombre, $intro,$materias){
		mysql_query("INSERT INTO examenes( nombre, intro) 
							VALUES ('$nombre','$intro')") or die(mysql_error());
		$idExamen=mysql_insert_id();
		foreach ($materias as $materia) {
			$sql = "INSERT INTO detalleMateria(idMateria,idExamen) 
							VALUES ($materia, $idExamen)";
							echo $sql;
			mysql_query($sql) or die(mysql_error());
		}
		header('Location: ../administracion/preguntas.php?id='.$idExamen);
	}
	function registrarPregunta ($pregunta, $respuestas, $idExamen, $correcto){
		$sql = "INSERT INTO preguntas(pregunta,idExamen) VALUES ('$pregunta',$idExamen)";
		mysql_query($sql);
		$idPregunta=mysql_insert_id();
		$i=1;
		foreach ($respuestas as $respuesta) {
			if ($i==$correcto){
				$consulta = "INSERT INTO respuestas(respuesta, valor, idPregunta) VALUES ('$respuesta', 1, $idPregunta)";
			} else {
				$consulta = "INSERT INTO respuestas(respuesta, idPregunta) VALUES ('$respuesta', $idPregunta)";
			}
			//echo $consulta;
			mysql_query($consulta) or die (mysql_error());
			$i+=1;
		}
		echo "OK";
	}

	function registraAlumno ($nControl){
		$sql = "INSERT INTO alumnos(nControl) VALUES ('$nControl')";
		mysql_query($sql) or die (mysql_error());
		//echo "OK";
	}

	function tablaDePreguntas($idExamen){
		echo '<table class="table table-stripped table-hover">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Preguntas</th>
		      <th>Respuestas</th>
		      <th>Acciones</th>
		    </tr>
		  </thead>
		  <tbody>';
		$i=1;
		$respuesta = mysql_query("SELECT * FROM preguntas WHERE idExamen=".$idExamen) or die(mysql_error());
		while ($pregunta=mysql_fetch_array($respuesta)){
			echo "<tr><td>".$i."</td><td>¿".$pregunta["pregunta"]."?</td><td>";
			
			$retorno = mysql_query("SELECT * FROM respuestas WHERE idPregunta=".$pregunta["id"]) or die(mysql_error());
			while ($celda=mysql_fetch_array($retorno)){
				if($celda["valor"]==1){
					echo "<span class='glyphicon glyphicon-ok'></span> ".$celda["respuesta"]."<br>";
				} else {
					echo "<span class='glyphicon glyphicon-remove'></span> ".$celda["respuesta"]."<br>";
				}
			}
			echo "</td><td><a class='eliminarPregunta btn btn-default' data-id='".$pregunta['id']."' href='#'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
			$i+=1;
		}  
		echo  '</tbody>
		</table>';
	}

	function tablaCalifica($idExamen, $idAlumno){
		echo '<table class="table table-stripped table-hover">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Preguntas</th>
		      <th>Respuestas</th>
		      <th>Seleccionada</th>
		    </tr>
		  </thead>
		  <tbody>';
		$i=1;
		$sql = "SELECT idPregunta, idRespuesta FROM respuestasAlumnos WHERE idExamen=".$idExamen." AND idAlumno=".$idAlumno;
		$respuesta = mysql_query($sql) or die(mysql_error());
		while ($pregunta=mysql_fetch_array($respuesta)){

			$nomPreg = $this->obtenerPregunta($pregunta["idPregunta"]);

			echo "<tr><td>".$i."</td><td>¿".$nomPreg."?</td><td>";
			
			$retorno = mysql_query("SELECT * FROM respuestas WHERE idPregunta=".$pregunta["idPregunta"]) or die(mysql_error());
			while ($celda=mysql_fetch_array($retorno)){
				if($celda["valor"]==1){
					echo "<span class='glyphicon glyphicon-ok'></span> ".$celda["respuesta"]."<br>";
				} else {
					echo "<span class='glyphicon glyphicon-remove'></span> ".$celda["respuesta"]."<br>";
				}
			}
			$nomResp = $this->obtenerRespuesta($pregunta["idRespuesta"]);
			if ($this->obtenerEstadoRespuesta($pregunta["idRespuesta"])) {
				echo "</td><td class='text-primary'><b>".$nomResp."</b></td>";
			} else {
				echo "</td><td class='text-danger'><b>".$nomResp."</b></td>";
			}
			
			$i+=1;
		}  
		echo  '</tbody>
		</table>';
	}

	function aciertos($idExamen, $idAlumno) {
		$i =0;
		$sql = "SELECT idPregunta, idRespuesta FROM respuestasAlumnos WHERE idExamen=".$idExamen." AND idAlumno=".$idAlumno;
		$respuesta = mysql_query($sql) or die(mysql_error());
		while ($pregunta=mysql_fetch_array($respuesta)){

			if ($this->obtenerEstadoRespuesta($pregunta["idRespuesta"])) {
				//echo "</td><td class='text-primary'><b>".$nomResp."</b></td>";
				$i+=1;
			} 
		}  
		echo $i;
	}
	function aciertosNum($idExamen, $idAlumno) {
		$i =0;
		$sql = "SELECT idPregunta, idRespuesta FROM respuestasAlumnos WHERE idExamen=".$idExamen." AND idAlumno=".$idAlumno;
		$respuesta = mysql_query($sql) or die(mysql_error());
		while ($pregunta=mysql_fetch_array($respuesta)){

			if ($this->obtenerEstadoRespuesta($pregunta["idRespuesta"])) {
				//echo "</td><td class='text-primary'><b>".$nomResp."</b></td>";
				$i+=1;
			} 
		}  
		return $i;
	}

	function total($idExamen, $idAlumno) {
		$sql = "SELECT count(id) as max FROM respuestasAlumnos WHERE idExamen=".$idExamen." AND idAlumno=".$idAlumno;
		$respuestaSql = mysql_query($sql);
		$celda = mysql_fetch_array($respuestaSql, MYSQL_ASSOC);
		echo $celda["max"];
	}

	function totalNum($idExamen, $idAlumno) {
		$sql = "SELECT count(id) as max FROM respuestasAlumnos WHERE idExamen=".$idExamen." AND idAlumno=".$idAlumno;
		$respuestaSql = mysql_query($sql);
		$celda = mysql_fetch_array($respuestaSql, MYSQL_ASSOC);
		return $celda["max"];
	}

	function califica($idExamen, $idAlumno) {
		$buenas = $this->aciertosNum($idExamen, $idAlumno);
		$totales = $this->totalNum($idExamen, $idAlumno);
		$calif = ($buenas/$totales)*100;
		echo $calif;
	}
	function calificaRet($idExamen, $idAlumno) {
		$buenas = $this->aciertosNum($idExamen, $idAlumno);
		$totales = $this->totalNum($idExamen, $idAlumno);
		$calif = ($buenas/$totales)*100;
		return $calif;
	}

	function mostrarExamenes($ids){
		echo '<table class="table table-stripped table-hover">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Examen</th>
		      <th>Materia</th>
		      <th>Acciones</th>
		    </tr>
		  </thead>
		  <tbody>';
		$i=1;
		$tokens = explode(',',$ids);
		foreach ($tokens as $token) {
			$nomMat = $this->obtenerNombreMateria($token);
		  	$retorno = mysql_query("SELECT * FROM detalleMateria WHERE idMateria=".$token) or die(mysql_error());
			while ($celda=mysql_fetch_array($retorno)){
				$nomEx = $this->obtenerNombreExamen($celda["idExamen"]);
				echo "<tr>
			  			<td>".$i."</td>
			  			<td>".$nomEx."</td>
			  			<td>".$nomMat."</td>
			  			<td>
			  				<a class='activarExamen btn btn-default' title='Permisos del examen' href='estado.php?id=".$celda["idExamen"]."'><span class='glyphicon glyphicon-time'></span></a>
			  				<a class='btn btn-default' title='Editar preguntas y respuestas' href='preguntas.php?id=".$celda["idExamen"]."'><span class='glyphicon glyphicon-list-alt'></span></a>
			  				<a class='btn btn-default' title='Informes del examen' href='informes.php?id=".$celda["idExamen"]."'><span class='glyphicon glyphicon-stats'></span></a>
			  				<a class='borrarExamen btn btn-default' data-matid='".$token."' data-id='".$celda["idExamen"]."' title='Eliminar examen' href='#'><span class='glyphicon glyphicon-trash'></span></a>
			  			</td>
			  		  </tr>";
			  	$i+=1;	
			}
		  }  
		echo  '</tbody>
		</table>';
	}

	function tablaInformes($idExamen) {
		echo '<table class="table table-stripped table-hover">
		  <thead>	
		    <tr>
		      <th>Alumno</th>
		      <th>Calificación total</th>
		      <th>Acciones</th>
		    </tr>
		  </thead>
		  <tbody>';
		$retorno = mysql_query("SELECT DISTINCT (idAlumno) FROM respuestasAlumnos WHERE idExamen =".$idExamen) or die(mysql_error());
		while ($celda=mysql_fetch_array($retorno, MYSQL_ASSOC)){ 
			$nomAlu= $this->obtenNombreAluRet($celda["idAlumno"]);
			$cals = number_format($this->calificaRet($idExamen, $celda["idAlumno"]));
			echo '<tr>
					<td>'.$nomAlu.'</td><td>'.$cals.'</td>
					<td><a class="btn btn-default" target="_blank" href="../califica.php?idExamen='.$idExamen.'&idAlumno='.$celda["idAlumno"].'"><span class="glyphicon glyphicon-th-list"></span> Desglosar</a></td>
				</tr>'	;
		}
		echo  '</tbody>
		</table>';
	}

	function obtenerNombreMateria($id) {
		$sql = "SELECT nombre FROM materias WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["nombre"];
	}

	function obtenerNombreExamen($id) {
		$sql = "SELECT nombre FROM examenes WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["nombre"];
	}

	function obtenerPregunta($id) {
		$sql = "SELECT pregunta FROM preguntas WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["pregunta"];
	}
	function obtenerRespuesta($id) {
		$sql = "SELECT respuesta FROM respuestas WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["respuesta"];
	}

	function obtenerEstadoRespuesta($id) {
		$sql = "SELECT valor FROM respuestas WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["valor"];
	}

	function contrasenaExamen($id) {
		$sql = "SELECT clave FROM examenes WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["clave"];
	}

	function estaActivo ($id){
		$sql = "SELECT estado FROM examenes WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["estado"];
	}

	function obtenerEstadoExamen($id) {
		$sql = "SELECT estado FROM examenes WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		if ($celda["estado"]){
			return 1;
		} else {
			return 0;
		}
	}

	function cambiarEstado ($id, $estado) {
		$clave = base_convert(mt_rand(0x1D39D3E06400000, 0x41C21CB8E0FFFFFF), 10, 36);
		if ($estado=="activo") {
			$sql = "UPDATE examenes SET estado=false WHERE id=".$id;
			$dos = "UPDATE examenes SET horaEvento=null WHERE id=".$id;
			$tres = 'UPDATE examenes SET clave="NOTSET" WHERE id='.$id;
		} else {
			$respuesta = mysql_query("SELECT TIME( NOW( ) ) AS hora") or die(mysql_error());
			$celda=mysql_fetch_array($respuesta);
			$sql = "UPDATE examenes SET estado=true WHERE id=".$id;
			$dos = "UPDATE examenes SET horaEvento='".$celda["hora"]."' WHERE id=".$id;
			$tres = 'UPDATE examenes SET clave="'.$clave.'" WHERE id='.$id;
		}
		mysql_query($sql) or die(mysql_error());
		mysql_query($dos) or die(mysql_error());
		mysql_query($tres) or die(mysql_error());
		echo "OK";
	}

	function selectorNumeroPreguntasMostrar ($id){
		$sql = "SELECT limite FROM examenes WHERE id=".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		$numeroDePreguntas = $this->numeroDePreguntas($id);
		if (is_null($celda["limite"])) {
			$limite = 0;
		} else {
			$limite = $celda["limite"];
		}
		echo "<select id='selectorNumeroPreg' data-id='".$id."' class='form-control'>";
		for ($i=0; $i<=$numeroDePreguntas; $i++){
			if ($limite==$i) {
				echo "<option selected>".$i."</option>";
			} else {
				echo "<option>".$i."</option>";
			}
		}
		echo "</select>";
	}

	function numeroDePreguntas($id){
		$sql = "SELECT COUNT( id ) AS maximo FROM preguntas WHERE idExamen =".$id;
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["maximo"];
	}

	function mostrarControlActivacionExamenes($id) {
		if ($this->obtenerEstadoExamen($id)){
			echo '<span class="subtitulo">Activo</span>
				  <a data-ahora="activo" data-id="'.$id.'" class="cambiarEstado btn btn-danger pull-right" href="#"><span style="font-size:15px;" class="glyphicon glyphicon-stop"></span> Desactivar</a>';
		} else {
			echo '<span class="subtitulo">Inactivo</span>
				  <a data-ahora="inactivo" data-id="'.$id.'" class="cambiarEstado btn btn-success pull-right" href="#"><span style="font-size:15px;" class="glyphicon glyphicon-play"></span> Activar</a>';
		}
	}

	function eliminarPregunta($id){
		mysql_query("DELETE FROM preguntas WHERE id=".$id) or die(mysql_error());
		mysql_query("DELETE FROM respuestas WHERE idPregunta=".$id) or die (mysql_error());
		echo "OK";
	}
	function eliminarExamen($id, $idMateria){
		mysql_query("DELETE FROM detalleMateria WHERE idExamen=".$id." AND idMateria=".$idMateria) or die(mysql_error());
		echo "OK";
	}

	function cambiarLimite ($id, $numero){
		mysql_query("UPDATE examenes SET limite=$numero WHERE id=".$id) or die(mysql_error());
		echo "OK";
	}

	function infoExamenes () {
		$resHora = mysql_query("SELECT TIME( NOW( ) ) AS hora") or die(mysql_error());
		$row=mysql_fetch_array($resHora);
		$horaActual = explode(':', $row["hora"]);
		$actualSumaSegundos = (($horaActual[0]*60)*60)+($horaActual[1]*60)+$horaActual[2];
		$retorno = mysql_query("SELECT * FROM examenes WHERE estado=1") or die(mysql_error());
		while ($celda=mysql_fetch_array($retorno)){

			$horaEvento = explode(':', $celda["horaEvento"]);
			$pasadoSumaSegundos = (($horaEvento[0]*60)*60)+($horaEvento[1]*60)+$horaEvento[2];
			$diferencia = ($actualSumaSegundos-$pasadoSumaSegundos);
			$aux1 = explode('.', ($diferencia/3600)."");
			$horas = $aux1[0];
			$minAux = (($diferencia/3600)-$aux1[0])*60; 
			$aux2 = explode('.', $minAux);
			$minutos = $aux2[0];
			$segCad ="".($aux2[1]*60);
			$segAux = str_split($segCad);
			$segundos = $segAux[0].$segAux[1];
			
			echo '<div class="cajasInfoActivos panel panel-primary" data-id="'.$celda["id"].'">
			<div class="panel-heading">'.$celda["nombre"].'</div>
			<div class="panel-body">
				<div id="row">
					<div class="col-md-9">
						<p class="contadores subtituloMini">Activo por: <span class="horas">'.$horas.'</span> hora(s) 
						<span class="minutos">'.$minutos.'</span> minuto(s) <span class="segundos">'.$segundos.'</span> segundo(s)</p> 
					</div>
					<div class="col-md-3">
						<a class="btn btn-default pull-right" href="estado.php?id='.$celda["id"].'"><span class="glyphicon glyphicon-time"></span> Permisos del examen</a>
					</div>
				</div>
			</div></div>';		
		}
	}

	function existeAlumno ($nControl){
		$sql = 'SELECT count(nControl) as total FROM alumnos WHERE nControl="'.$nControl.'" ';
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		if ($celda["total"]!=0) {
			return true;
		} else {
			return false;
		}
	}
	function obtenerIdAlumno ($nControl) {
		$sql = 'SELECT id FROM alumnos WHERE nControl="'.$nControl.'" ';
		$respuesta = mysql_query($sql) or die(mysql_error());
		$celda=mysql_fetch_array($respuesta);
		return $celda["id"];		
	}
}

?>