<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de aplicación de examenes">
    <meta name="author" content="Cristian Orihuela Torres">
    <title>AcadTest - Sistema de aplicación de exámenes en línea</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).on("ready", iniciar);

    function iniciar() {
    	
    	$("#menu li").each(function(){
    		href = $(this).find('a').attr('href');
    		ruta = window.location.pathname;
    		var index = ruta.lastIndexOf("/") + 1;
			var filename = ruta.substr(index);
		    if (href == filename) {
		      $(this).addClass('active');
		    } 
    	});

    	$("#linkSalir").on("click", cerrarSesion);

    }

    function cerrarSesion() {
    	$.get("../motor/ajax.php?seccion=cerrarSesion", function(){
    		window.location = "index.php";
    	});
    }

    </script>
</head>
<body>
	<section>
		<article class="row">
			<div class="col-md-3">
				<div id="barraLateral" class="container">
					<br>
					<p class="text-center">
						<img class="img-thumbnail" width="90%" src="../imagenes/examen.jpg">
					</p>
					<p class="subtitulo text-center">
						Menú de navegación
					</p>
					<ul id="menu" class="nav nav-pills nav-stacked">
	  					<li><a href="panel.php">Inicio</a></li>
	  					<li><a href="registrarExamen.php">Nuevo examen</a></li>
	  					<li><a href="adminExamen.php">Administrador de exámenes</a></li>
	  					<li><a id="linkSalir" href="#">Cerrar sesión</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-9">
				<div id="contenido">